/* *********************************************** *
 * Найти в массиве элемент, наиболее близкий       *
 *       к среднему арифметическому                *
 *                суммы его элементов              *
 * *********************************************** */

#include <stdio.h>

#define MX_SYMBOLS 300 /* Максимальное количество вводимых символов */
#define MX_NUMBERS 50  /* Максимальное количество возможных десятичных чисел */

int igetline( char[], int );
int iSplit( char[], int[], int, int ); /* Возвращает количество записанных элементов (sic!) */
int itod( char[] );
int isNumber( char );

double dMedium( int[], int );
double dabs( double );

void vClean( char[] );

int main(  ){
	
	char line[MX_SYMBOLS]; /* Строка, где хранится вводимая информация */
	
	int iNumbers[MX_NUMBERS]; /* Массив, куда будут записываться числа */
	int iHowManyNumbers;      /* Общее количество введённых чисел ( чисел в массиве ) */
	int index=0;              /* Индекс массива в котором хранится искомое значение */
	
	double dNumber; /* Среднее арифметическое */
	double delta;   /* Разница между средним значением и элементом массива */
	
	printf( "Please, enter your numbers (max %d characters & max %d numbers)\n  ->", MX_SYMBOLS, MX_NUMBERS );
	
	igetline( line, MX_SYMBOLS );
	vClean( line );
	
	printf( "The line is : \"%s\"\n", line );
	
	iHowManyNumbers = iSplit( line, iNumbers, MX_SYMBOLS, MX_NUMBERS );
	
	dNumber = dMedium( iNumbers, iHowManyNumbers );
	
	printf( "Medium is : %f\n", dNumber );
	
	delta = dabs( (double)iNumbers[index] - dNumber );
	
	for( int i=0; i < iHowManyNumbers; i++ ){
		
		if ( dabs( (double)iNumbers[i] - dNumber ) < delta ) { index = i; delta = dabs( (double)iNumbers[i] - dNumber ); }
		printf( "Number[%2d] = %d\tDelta =\t%.2f\n", i, iNumbers[i], dabs( (double)iNumbers[i] - dNumber ) );
		
	}
	
	printf( "Your number is : \"%d\"\n", iNumbers[index] );
	
	return 0;
	
}

int igetline( char s[], int limit ){
	
	/* ********************************* *
	 * Считывает символы ASCII, при      *
	 *    этом убирая всё, что не цифра  *
	 *              или знак '-'         *
	 * ********************************* */
	
	char c;
	
	int i = 0;
	
	while( (c=getchar()) == '\t' || c == ' ' || isNumber( c ) );
	s[i++] = c;
	
	for( ; (c=getchar()) != '\n' && i<limit ; i++ ){
		if ( c == '\t' || isNumber( c ) ) s[i] = ' ';
		else s[i] = c;
	}
	
	s[i] = '\0';
	
	return i;
	
}

int iSplit( char s[], int iArray[], int limit_symbols, int limit_numbers ){
	
	/* *************************************** *
	 * Разбивает строку на подстроки,          *
	 *     Затем преобразую подстроки в числа  *
	 *               с помощью функции itod()  *
	 * *************************************** */
	
	int i=0; /* счётчик индекса строки */
	int j=0; /* счётчик индекса подстроки */
	int k=0; /* счётчик десятичных чисел */
	
	char sTemp[limit_symbols]; /* Подстрока, преобразующаяся в число */
	
	while( s[i] != '\0' && k < limit_numbers ){
		
		if (s[i] != ' ') sTemp[j++] = s[i];
		else {
			
			sTemp[j] = '\0';
			iArray[k++] = itod( sTemp ); 
			j=0; 
			
		}
		
		i++;
		
	}

	if ( j != 0 ){
		sTemp[j] = '\0';
		iArray[k] = itod( sTemp );
	}
	else k--;
	
	return ++k;
	
}

int itod( char s[] ){
	
	/* ************************************** *
	 * Превращает строку в десятичное число   *
	 * ************************************** */
	
	int i=0;
	int sign = 1;/* Знак числа, по умолчанию положительный */
	int result=0;
	
	if ( s[i] == '-' ) { sign = -1; i++; }
	
	while( s[i+1] != '\0' ){
		
		result += sign*( s[i++] - '0' );
		result *= 10;
		
	}
	
	result += sign*( s[i] - '0' );
	
	return result;
	
}

int isNumber( char c ){
	
	/* ********************************** *
	 * Проверяет, является ли символ      *
	 *         цифрой или знаком '-'      *
	 * ********************************** */
	
	if ( ( c <= '9' && c >= '0' ) || c =='-' ) { return 0; }
	else { return 1; }
	
}

void vClean( char s[] ){
	
	/* **************************************** *
	 * Убирает лишние пробелы и знаки 'минус'   *
	 * **************************************** */
	
	int i=0;
	int j=0;
	
	while( s[i] != '\0' ){
		
		if ( s[i] == ' ' && isNumber( s[i+1] ) ) i++;
		else if ( s[i] == '-' && ( s[i+1] == ' ' || s[i+1] == '-' ) ) i++;
		else s[j++] = s[i++];
		
	}
	
	s[j] = '\0';
	
}

double dMedium( int iArray[], int iNumber ){
	
	/* ****************************** *
	 * Считает среднее арифметическое *
	 * ****************************** */
	
	int iSum=0;
	
	for( int i=0; i < iNumber; i++ ){
		iSum += iArray[i];
	}
	
	return (double)iSum/iNumber;
	
}

double dabs( double delta ){
	
	/* ****************************** *
	 * Возвращает абсолютное          *
	 *     значение вещественного     *
	 *                   аргумента    *
	 * ****************************** */
	
	if ( delta < 0 ) return -delta;
	else return delta;
	
}
